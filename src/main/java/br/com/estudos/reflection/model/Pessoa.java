package br.com.estudos.reflection.model;

public class Pessoa {
	
	protected static String nome;
	protected Integer idade;
	private String endereco;
	public static Double altura;
	
	protected static String getNome() {
		return nome;
	}
	public static void setNome(String nome) {
		Pessoa.nome = nome;
	}
	public final Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	private String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public static Double getAltura() {
		return altura;
	}
	public static void setAltura(Double altura) {
		Pessoa.altura = altura;
	}
	
}
