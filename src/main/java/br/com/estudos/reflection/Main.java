package br.com.estudos.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import br.com.estudos.reflection.model.Pessoa;

public class Main {

	public static void main(String[] args) {
		
		Class clazz = Pessoa.class;
		System.out.println("Imprimindo os nomes dos atributos da classe : " + clazz.getSimpleName());
		for(Field field : clazz.getDeclaredFields()) {
			System.out.println("nome do atribute: " + field.getName());
			System.out.println("com os modificadores de acesso: " + Modifier.toString(field.getModifiers()));
			
		}
		System.out.println("imprimindo os métodos da classe");
		for(Method method : clazz.getDeclaredMethods()) {
			System.out.println("nome do método: " + method.getName());
			System.out.println("com os modificadores: " + Modifier.toString(method.getModifiers()));
		}
	}

}
